<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1 >Sum of Natural Numbers</h1>
<form method="GET">
    <input type="number" name="nnumber">
    <button>Submit</button>
</form>

<?php

$nnum = isset($_GET["nnumber"]) ? $_GET["nnumber"] : 0;

$sum=0;
for($i=0; $i<=$nnum; $i++){
    $sum= $sum+$i;
}
echo "Sum of $nnum numbers is: $sum";
?>

</body>
</html>